#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#define SENSOR  D1

long currentMillis = 0;
long previousMillis = 0;
int interval = 1000;
float calibrationFactor = 4.5;
volatile byte pulseCount;
byte pulse1Sec = 0;
float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;
String f;

const char* ssid = "@wifi.id";
const char* password = "jenengku";
const char* host = "ns02.000webhost.com";


void IRAM_ATTR pulseCounter()
{
  pulseCount++;
}

void setup() {
  Serial.begin(115200);
  
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED){
    Serial.println("Mencari jaringan...");
    delay(500);
  }
  Serial.println("Wifi Tersambung");
  
  pinMode(SENSOR, INPUT_PULLUP);
  pulseCount = 0;
  flowRate = 0.0;
  flowMilliLitres = 0;
  totalMilliLitres = 0;
  previousMillis = 0;
  attachInterrupt(digitalPinToInterrupt(SENSOR), pulseCounter, FALLING);

}

void loop() {
  currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    pulse1Sec = pulseCount;
    pulseCount = 0;
    flowRate = ((1000.0 / (millis() - previousMillis)) * pulse1Sec) / calibrationFactor;
    previousMillis = millis();
    flowMilliLitres = (flowRate / 60) * 1000;
    totalMilliLitres += flowMilliLitres;
    
    
    // Print the flow rate for; this second in litres / minute
    Serial.print("Air Keluar: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print("L/menit");
    Serial.print("\t");       // Print tab space
    
    // Print the cumulative total of litres flowed since starting
    Serial.print("Total Air Keluar: ");
    Serial.print(totalMilliLitres);
    Serial.print("mL / ");
    Serial.print(totalMilliLitres / 1000);
    Serial.println("L");

    WiFiClient client;
    if(!client.connect(host, 3306)){
      Serial.println("tidak terkoneksi server");
      delay(500);
      return;
    }

    f = String(flowRate);
    if(int(flowRate) > 0 ){
      String Link2;
      String li = "nilai=" + String(totalMilliLitres / 1000) + "&" + "rpm=" + String(f) + "&" + "ml=" + String(totalMilliLitres);      
      
      HTTPClient http;
      Link2 = "http://alfianapps.000webhostapp.com/api_monitoring/kirimdata.php?"+ String(li);
      http.begin(Link2);
      http.GET();
      http.end();
    }
    
  }
  
}
