volatile int NbTopsFan; // mengukur tepi naik dari sinyal
int Calc;                               
int hallsensor = 2;

void  rpm (){      // Ini adalah fungsi yang dipanggil oleh interupt
  NbTopsFan ++;  // Fungsi ini mengukur tepi naik dan turun sinyal sensor efek hall
} 

void setup() {
  pinMode(hallsensor, INPUT); // menginisialisasi pin 2 digital sebagai masukan
  Serial.begin(9600);

  attachInterrupt(0, rpm, RISING);
}

void loop() {
   NbTopsFan = 0;      // Set NbTops ke 0 siap untuk kalkulasi
   sei();              // Mengaktifkan interupsi
   delay (1000);      
   cli();              // Nonaktifkan interupsi
   Calc = (NbTopsFan * 60 / 7.5); // (Frekuensi pulsa x 60) / 7.5Q, = laju aliran dalam L / jam 
  
  
   Serial.print (Calc, DEC); // Mencetak angka yang dihitung di atas
   Serial.print (" L/hour\r\n"); // Mencetak "L / jam" dan mengembalikan baris baru
}
