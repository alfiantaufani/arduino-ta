//#include <Arduino.h> 
//#define USE_SERIAL Serial
//#include <ESP8266WiFi.h> 
//#include <ESP8266HTTPClient.h>

// LcD library ne
//#include <LiquidCrystal_I2C.h>
//LiquidCrystal_I2C lcd(0x27, 16, 2);

volatile byte NbTopsFan; // mengukur tepi naik dari sinyal
float Calc;                               
byte hallsensor = 4;

void  rpm (){      // Ini adalah fungsi yang dipanggil oleh interupt
  NbTopsFan ++;  // Fungsi ini mengukur tepi naik dan turun sinyal sensor efek hall
} 
void ICACHE_RAM_ATTR ISRoutine(){
  NbTopsFan ++; 
}
void setup() {
  pinMode(hallsensor, INPUT); // menginisialisasi pin 2 digital sebagai masukan
  Serial.begin(115200);
  
  attachInterrupt(hallsensor, ISRoutine, FALLING);
  //attachInterrupt(0, rpm, RISING);
}

void loop() {
  detachInterrupt(hallsensor);
        // Set NbTops ke 0 siap untuk kalkulasi
   //sei();              // Mengaktifkan interupsi
   delay (1000);      
   //cli();              // Nonaktifkan interupsi
   Calc = (NbTopsFan * 60 / 7.5); // (Frekuensi pulsa x 60) / 7.5Q, = laju aliran dalam L / jam 
  
   Serial.print(int(Calc)); // Mencetak angka yang dihitung di atas
   Serial.print(" Liter/hour\r\n"); // Mencetak "L / jam" dan mengembalikan baris baru 
   NbTopsFan = 0;
   attachInterrupt(hallsensor, ISRoutine, FALLING);
}
